<?php

namespace GetNoticed\BlockTestEnvironments\Observer;

use Magento\Framework\ {
    App\RequestInterface,
    App\Request\Http as HttpRequest,
    App\State,
    App\Area,
    App\Response\Http as HttpResponse,
    Controller\ResultFactory,
    Event\Observer,
    Event\ObserverInterface,
    Exception\LocalizedException,
    HTTP\PhpEnvironment\RemoteAddress,
    Stdlib\CookieManagerInterface
};
use GetNoticed\BlockTestEnvironments\ {
    App\TestMode,
    Controller\Adminhtml\Bte\Frontend
};
use Zend\Http\{
    Header\HeaderInterface
};

class ControllerFrontSendResponseBefore implements ObserverInterface
{

    const HEADER_KEY_ENABLED = 'X-BTE-Enabled';
    const HEADER_KEY_BLOCKED = 'X-BTE-Blocked';
    const HEADER_KEY_ALLOWED_REASON = 'X-BTE-Allowed-Reason';

    /**
     * @var HttpResponse
     */
    protected $response;

    // DI

    /**
     * @var TestMode
     */
    private $testMode;

    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var State
     */
    private $state;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var RequestInterface|HttpRequest
     */
    private $request;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    public function __construct(
        TestMode $testMode,
        RemoteAddress $remoteAddress,
        State $state,
        ResultFactory $resultFactory,
        RequestInterface $request,
        CookieManagerInterface $cookieManager
    ) {
        $this->testMode = $testMode;
        $this->remoteAddress = $remoteAddress;
        $this->state = $state;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        // If test mode is disabled, do not process further
        if ($this->testMode->isOn() !== true) {
            return;
        }

        // Get response for controller
        /** @var HttpResponse $response */
        $response = $observer->getData('response');

        $this->setIsEnabled($response, true);
        $this->setIsBlocked($response, true);
        $this->getVisitorAccessStatus($response);

        if ($this->isEnabled($response) && $this->isBlocked($response)) {
            $response->setHeader('Cache-Control', 'no-cache');

            /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
            $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirect->setPath('getnoticed/bte/blocked');
            $redirect->renderResult($response);
        }
    }

    /**
     * @param HttpResponse $response
     */
    protected function getVisitorAccessStatus(HttpResponse $response): void
    {
        // If a special admin authorization cookie is set, show the webshop
        if ($this->cookieManager->getCookie(Frontend::COOKIE_NAME) === '1') {
            $this->setIsBlocked($response, false);
            $this->setAllowedReason($response, 'Frontend authorization cookie found.');

            return;
        }

        // If the area is not frontend, we will not process
        try {
            $areaCode = $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $areaCode = null;
        }

        // If the area code is null or if it's not AREA_FRONTEND, user is allowed
        if ($areaCode === null || $areaCode !== Area::AREA_FRONTEND) {
            $this->setIsBlocked($response, false);
            $this->setAllowedReason($response, 'Area is not set or area is not frontend');

            return;
        }

        // If the page is the "blocked" page, allow the user to see it
        if (preg_match('/(getnoticed)\/(bte)\/(blocked)/', $this->request->getPathInfo())) {
            $this->setIsBlocked($response, false);
            $this->setAllowedReason($response, 'User is blocked information page');

            return;
        }

        // Check if the user is whitelisted, if the user is allowed, continue processing
        if ($this->testMode->isOn($this->remoteAddress->getRemoteAddress()) === false) {
            $this->setIsBlocked($response, false);
            $this->setAllowedReason($response, 'User is whitelisted by IP');

            return;
        }
    }

    private function isEnabled(HttpResponse $response): bool
    {
        $header = $response->getHeader(self::HEADER_KEY_ENABLED);

        return $header instanceof HeaderInterface && $header->getFieldValue() === 'Yes';
    }

    private function setIsEnabled(HttpResponse $response, bool $isEnabled)
    {
        $response->setHeader(self::HEADER_KEY_ENABLED, $isEnabled ? 'Yes' : 'No', true);
    }

    private function isBlocked(HttpResponse $response): bool
    {
        $header = $response->getHeader(self::HEADER_KEY_BLOCKED);

        return $header instanceof HeaderInterface && $header->getFieldValue() === 'Yes';
    }

    private function setIsBlocked(HttpResponse $response, bool $isBlocked)
    {
        $response->setHeader(self::HEADER_KEY_BLOCKED, $isBlocked ? 'Yes' : 'No', true);
    }

    private function setAllowedReason(HttpResponse $response, string $reason)
    {
        $response->setHeader(self::HEADER_KEY_ALLOWED_REASON, $reason, true);
    }
}
