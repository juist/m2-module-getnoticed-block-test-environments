<?php

namespace GetNoticed\BlockTestEnvironments\Block;

use Magento\Framework\{
    App\RequestInterface,
    App\Request\Http as HttpRequest,
    View\Element\Block\ArgumentInterface,
    HTTP\PhpEnvironment\RemoteAddress
};
use Magento\Backend\{
    Model\UrlInterface
};
use Magento\Store\{
    Model\StoreManagerInterface
};

class BlockedPage implements ArgumentInterface
{
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var RequestInterface|HttpRequest
     */
    private $request;

    /**
     * @var UrlInterface
     */
    private $adminUrl;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        RemoteAddress $remoteAddress,
        RequestInterface $request,
        UrlInterface $adminUrl,
        StoreManagerInterface $storeManager
    ) {
        $this->remoteAddress = $remoteAddress;
        $this->request = $request;
        $this->adminUrl = $adminUrl;
        $this->storeManager = $storeManager;
    }

    public function getHostname(): string
    {
        return $this->request->getHttpHost();
    }

    public function getRemoteAddress(): string
    {
        return $this->remoteAddress->getRemoteAddress();
    }

    public function getAdminUrl(): string
    {
        return $this->adminUrl->getUrl(
            'getnoticed/bte/frontend',
            ['store_id' => $this->storeManager->getStore()->getId()]
        );
    }
}
