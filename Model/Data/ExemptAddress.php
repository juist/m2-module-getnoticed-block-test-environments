<?php

namespace GetNoticed\BlockTestEnvironments\Model\Data;

class ExemptAddress
{
    /**
     * @var string
     */
    private $ipAddress;

    /**
     * @var string
     */
    private $label;

    /**
     * @var bool
     */
    private $isDefault = false;

    public function __construct(string $ipAddress, string $label, bool $isDefault = false)
    {
        $this->ipAddress = $ipAddress;
        $this->label = $label;
        $this->isDefault = $isDefault;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     *
     * @return ExemptAddress
     */
    public function setIpAddress(string $ipAddress): ExemptAddress
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return ExemptAddress
     */
    public function setLabel(string $label): ExemptAddress
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     *
     * @return ExemptAddress
     */
    public function setIsDefault(bool $isDefault): ExemptAddress
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'ipAddress' => $this->getIpAddress(),
            'label'     => $this->getLabel()
        ];
    }
}
