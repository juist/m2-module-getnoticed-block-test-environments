<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use Magento\Setup\ {
    Validator\IpValidator
};
use GetNoticed\BlockTestEnvironments\ {
    App\TestMode
};
use Symfony\Component\Console\ {
    Command\Command
};

abstract class AbstractTestModeCommand extends Command
{
    /**
     * @var TestMode
     */
    protected $testMode;

    /**
     * @var IpValidator
     */
    protected $ipValidator;

    public function __construct(
        TestMode $testMode,
        IpValidator $ipValidator
    ) {
        parent::__construct();

        $this->testMode = $testMode;
        $this->ipValidator = $ipValidator;
    }
}
