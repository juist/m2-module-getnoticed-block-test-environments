<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use GetNoticed\BlockTestEnvironments\{
    App\TestMode,
    Model\Data\ExemptAddress
};
use Magento\Framework\Exception\FileSystemException;
use Symfony\Component\Console\{
    Input\InputArgument,
    Input\InputDefinition,
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class RemoveTestModeWhitelistCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:whitelist:remove')
            ->setDescription('Removes an exempted IP-address, so they can\'t view the webshop.')
            ->setHelp('Run this command to remove an exemption on an IP-address, so they can\'t view the webshop.')
            ->setDefinition(
                new InputDefinition(
                    [
                        new InputArgument('ip-address', InputArgument::REQUIRED, 'IP-address to remove from exemption.')
                    ]
                )
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn() !== true) {
            $io->note(
                __('Test mode is currently not enabled, the removed IP-address will have no effect.')
            );
        }

        if ($this->testMode->isDefaultIp($input->getArgument('ip-address'))) {
            $io->error(__('Default IP-addresses can not be dynamically removed.'));
        } elseif ($this->testMode->isIpExempt($input->getArgument('ip-address'), false)) {
            $io->error(__('This IP is not currently exempt.'));
        } else {
            try {
                if ($this->testMode->removeExemptAddress($input->getArgument('ip-address'))) {
                    $io->success(__('IP address successfully removed from exemption.'));
                } else {
                    $io->error(__('Unable to remove exemption on IP-address.'));
                }
            } catch (FileSystemException $e) {
                $io->error(__('Unable to remove exemption on IP-address: %1', $e->getMessage()));
            }
        }
    }
}
