<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use Magento\Framework\ {
    Exception\FileSystemException
};
use Symfony\Component\Console\ {
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class DisableTestModeCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:test-mode:disable')
            ->setDescription('Disables test mode for this installation.')
            ->setHelp('Run this command to disable test mode.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn() !== true) {
            $io->note(__('Test mode is already disabled.'));
        } else {
            try {
                if ($this->testMode->set(false)) {
                    $io->success(__('Test mode successfully disabled.'));
                } else {
                    $io->error(__('Error during enabling test mode.'));
                }
            } catch (FileSystemException $e) {
                $io->error(__('Error during enabling test mode: %1', $e->getMessage()));
            }
        }
    }
}
