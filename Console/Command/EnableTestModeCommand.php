<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use Magento\Framework\ {
    Exception\FileSystemException
};
use Symfony\Component\Console\ {
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class EnableTestModeCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:test-mode:enable')
            ->setDescription('Enables test mode for this installation.')
            ->setHelp('Run this command to enable test mode, remember to set IP-addresses for exemption.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn()) {
            $io->note(__('Test mode is already enabled.'));
        } else {
            try {
                if ($this->testMode->set(true)) {
                    $io->success(__('Test mode successfully enabled.'));
                } else {
                    $io->error(__('Error during enabled test mode.'));
                }
            } catch (FileSystemException $e) {
                $io->error(__('Error during enabling test mode: %1', $e->getMessage()));
            }
        }
    }
}
