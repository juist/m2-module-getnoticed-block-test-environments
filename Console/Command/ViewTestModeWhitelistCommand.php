<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use GetNoticed\BlockTestEnvironments\{
    App\TestMode,
    Model\Data\ExemptAddress
};
use Symfony\Component\Console\ {
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class ViewTestModeWhitelistCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:whitelist:view')
            ->setDescription('Shows all the currently exempt IP-addresses.')
            ->setHelp('Run this command to view the currently exempt IP-addresses.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn() !== true) {
            $io->note(__('Test mode is currently not enabled, this list will have no effect.'));
        }

        $addressInfo = $this->testMode->getAddressInfo(true);

        if (empty($addressInfo)) {
            $io->note(__('IP whitelist is currently empty, no one will be able to access the webshop.'));
        } else {
            $io->table(
                [__('IP-address'), __('Label'), __('Default')],
                array_map(
                    function (ExemptAddress $address) {
                        return [
                            $address->getIpAddress(),
                            $address->getLabel(),
                            $address->isDefault() ? __('Yes') : __('No')
                        ];
                    },
                    $addressInfo
                )
            );
        }
    }
}
