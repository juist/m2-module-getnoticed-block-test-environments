<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use Symfony\Component\Console\ {
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class StatusTestModeCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:test-mode:status')
            ->setDescription('Returns test mode status for this installation.')
            ->setHelp('Run this command to view the current test status.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn()) {
            $io->success(__('Test mode is currently enabled.'));
        } else {
            $io->note(__('Test mode is currently disabled.'));
        }
    }
}
