<?php

namespace GetNoticed\BlockTestEnvironments\Console\Command;

use GetNoticed\BlockTestEnvironments\{
    App\TestMode,
    Model\Data\ExemptAddress
};
use Magento\Framework\Exception\FileSystemException;
use Symfony\Component\Console\{
    Input\InputArgument,
    Input\InputDefinition,
    Input\InputInterface,
    Output\OutputInterface,
    Style\SymfonyStyle
};

class AddTestModeWhitelistCommand extends AbstractTestModeCommand
{
    protected function configure()
    {
        $this
            ->setName('bte:whitelist:add')
            ->setDescription('Exempts a new IP-address, so they can view the webshop.')
            ->setHelp('Run this command to exempt a new IP-address, so they can view the webshop.')
            ->setDefinition(
                new InputDefinition(
                    [
                        new InputArgument('ip-address', InputArgument::REQUIRED, 'IP-address to exempt.'),
                        new InputArgument('label', InputArgument::REQUIRED, 'Label which describes the IP-address.')
                    ]
                )
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->testMode->isOn() !== true) {
            $io->note(
                __('Test mode is currently not enabled, the newly added/updated IP-address will have no effect.')
            );
        }

        try {
            if ($this->testMode->updateExemptAddress($input->getArgument('ip-address'), $input->getArgument('label'))) {
                $io->success(__('IP address successfully exempt from test mode blockage.'));
            } else {
                $io->error(__('Unable to exempt IP-address.'));
            }
        } catch (FileSystemException $e) {
            $io->error(__('Unable to exempt IP-address: %1', $e->getMessage()));
        }
    }
}
