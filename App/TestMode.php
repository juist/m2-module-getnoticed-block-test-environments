<?php

namespace GetNoticed\BlockTestEnvironments\App;

use Magento\Framework\{
    App\Filesystem\DirectoryList,
    Exception\FileSystemException,
    Filesystem,
    Filesystem\Directory\WriteInterface,
    Serialize\Serializer\Json
};
use GetNoticed\BlockTestEnvironments\ {
    Model\Data\ExemptAddress,
    Model\Data\ExemptAddressFactory
};

class TestMode
{
    const FLAG_FILENAME = '.test-environment.flag';
    const IP_FILENAME = '.test-environment.ip';
    const FLAG_DIR = DirectoryList::VAR_DIR;

    /**
     * @var WriteInterface
     */
    private $flagDir;

    /**
     * @var ExemptAddress[]
     */
    private $addressInfo;

    // DI

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var ExemptAddressFactory
     */
    private $exemptAddressFactory;

    /**
     * @var ExemptAddress[]
     */
    private $defaultExemptAddresses = [];

    public function __construct(
        Filesystem $filesystem,
        Json $serializer,
        ExemptAddressFactory $exemptAddressFactory,
        array $defaultExemptAddresses = []
    ) {
        $this->filesystem = $filesystem;
        $this->serializer = $serializer;
        $this->exemptAddressFactory = $exemptAddressFactory;

        foreach ($defaultExemptAddresses as $addressData) {
            $this->defaultExemptAddresses[] = $this->exemptAddressFactory->create(['isDefault' => true] + $addressData);
        }
    }

    /**
     * @param string|null $remoteAddress
     *
     * @return bool
     */
    public function isOn(?string $remoteAddress = null)
    {
        // If the file flag does not exist, test mode is not enabled.
        if ($this->getFileFlag() !== true) {
            return false;
        }

        // If no remote address was specified, return true now
        if ($remoteAddress === null) {
            return true;
        }

        // Otherwise, compare the given IP against our list.
        // If the IP is in our list, test mode is off, otherwise it is on.
        return $this->isIpExempt($remoteAddress);
    }

    /**
     * @param string $remoteAddress
     * @param bool   $includeDefault
     *
     * @return bool
     */
    public function isIpExempt(string $remoteAddress, bool $includeDefault = true): bool
    {
        return in_array($remoteAddress, $this->getExemptIpAddresses($includeDefault)) ? false : true;
    }

    /**
     * @param string $remoteAddress
     *
     * @return bool
     */
    public function isDefaultIp(string $remoteAddress): bool
    {
        foreach ($this->defaultExemptAddresses as $defaultExemptAddress) {
            if ($defaultExemptAddress->getIpAddress() === $remoteAddress) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param bool $testMode
     *
     * @return bool
     * @throws FileSystemException
     */
    public function set(bool $testMode): bool
    {
        if ($testMode === true && $this->isOn() === false) {
            return $this->getFlagDirWrite()->touch(self::FLAG_FILENAME);
        } elseif ($testMode === false && $this->isOn() === true) {
            return $this->getFlagDirWrite()->delete(self::FLAG_FILENAME);
        }

        return true;
    }

    /**
     * Returns a list of all exempt IP-addresses.
     *
     * @param bool $includeDefault
     *
     * @return ExemptAddress[]
     */
    public function getAddressInfo(bool $includeDefault = false): array
    {
        if ($this->addressInfo !== null) {
            return array_merge($includeDefault ? $this->defaultExemptAddresses : [], $this->addressInfo);
        }

        $this->addressInfo = [];

        try {
            $ipFile = $this->getFlagDirWrite()->readFile(self::IP_FILENAME);
        } catch (FileSystemException $e) {
            $this->addressInfo = [];

            return array_merge($includeDefault ? $this->defaultExemptAddresses : [], $this->addressInfo);
        }

        try {
            $data = $this->serializer->unserialize($ipFile);
        } catch (\InvalidArgumentException $e) {
            $this->addressInfo = [];

            return array_merge($includeDefault ? $this->defaultExemptAddresses : [], $this->addressInfo);
        }

        $this->addressInfo = array_map(
            function (array $record) {
                return $this->exemptAddressFactory->create($record);
            },
            $data
        );

        return array_merge($includeDefault ? $this->defaultExemptAddresses : [], $this->addressInfo);
    }

    /**
     * @param bool $includeDefault
     *
     * @return array
     */
    public function getExemptIpAddresses(bool $includeDefault = false): array
    {
        return array_map(
            function (ExemptAddress $address) {
                return $address->getIpAddress();
            },
            $this->getAddressInfo($includeDefault)
        );
    }

    /**
     * @param string $ipAddress
     * @param string $label
     *
     * @return bool
     * @throws FileSystemException
     */
    public function updateExemptAddress(string $ipAddress, string $label): bool
    {
        $addressInfo = $this->getAddressInfo();

        foreach ($addressInfo as &$address) {
            if ($address->getIpAddress() === $ipAddress) {
                $address->setLabel($label);

                return $this->updateAddressInfoFile($addressInfo);
            }
        }

        $addressInfo[] = $this->exemptAddressFactory->create(['ipAddress' => $ipAddress, 'label' => $label]);

        return $this->updateAddressInfoFile($addressInfo);
    }

    /**
     * @param string $ipAddress
     *
     * @return bool
     * @throws FileSystemException
     */
    public function removeExemptAddress(string $ipAddress): bool
    {
        $addressInfo = $this->getAddressInfo();

        foreach ($addressInfo as $idx => $address) {
            if ($address->getIpAddress() === $ipAddress) {
                unset($addressInfo[$idx]);
            }
        }

        return $this->updateAddressInfoFile($addressInfo);
    }

    /**
     * @param ExemptAddress[] $addressInfo
     *
     * @return bool
     * @throws FileSystemException
     */
    public function updateAddressInfoFile(array $addressInfo): bool
    {
        // Forces reload on next request
        $this->addressInfo = null;

        return $this->getFlagDirWrite()->writeFile(
            self::IP_FILENAME,
            $this->serializer->serialize(
                array_map(
                    function (ExemptAddress $exemptAddress) {
                        return $exemptAddress->toArray();
                    },
                    $addressInfo
                )
            )
        );
    }

    /**
     * @return WriteInterface
     * @throws FileSystemException
     */
    protected function getFlagDirWrite(): WriteInterface
    {
        if ($this->flagDir instanceof WriteInterface) {
            return $this->flagDir;
        }

        return $this->flagDir = $this->filesystem->getDirectoryWrite(self::FLAG_DIR);
    }

    /**
     * @return bool
     */
    protected function getFileFlag(): bool
    {
        try {
            return $this->getFlagDirWrite()->isExist(self::FLAG_FILENAME);
        } catch (FileSystemException $e) {
            // For safety purposes, if an error occurs in obtaining the flag, we will assume it exists.
            return true;
        }
    }
}
