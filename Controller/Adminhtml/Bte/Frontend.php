<?php

namespace GetNoticed\BlockTestEnvironments\Controller\Adminhtml\Bte;

use Magento\Framework\ {
    Stdlib\Cookie\PublicCookieMetadataFactory,
    UrlInterface,
    Stdlib\CookieManagerInterface,
    Controller\ResultFactory};
use Magento\Backend\ {
    App\Action,
    App\Action\Context
};

class Frontend extends Action
{
    const COOKIE_NAME = 'temporary_frontend_access';

    /**
     * @var UrlInterface
     */
    private $frontendUrl;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var PublicCookieMetadataFactory
     */
    private $cookieMetadataFactory;

    public function __construct(
        Context $context,
        UrlInterface $frontendUrl,
        CookieManagerInterface $cookieManager,
        PublicCookieMetadataFactory $cookieMetadataFactory
    ) {
        parent::__construct($context);

        $this->frontendUrl = $frontendUrl;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        try {
            $metadata = $this->cookieMetadataFactory->create();
            $metadata->setPath('/');

            $this->cookieManager->setPublicCookie(self::COOKIE_NAME, true, $metadata);

            $redirect->setUrl($this->frontendUrl->getUrl('/'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Unable to set frontend cookie - please contact support.'));

            $redirect->setUrl($this->_backendUrl->getUrl('admin/dashboard'));
        }

        return $redirect;
    }
}