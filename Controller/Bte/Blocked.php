<?php

namespace GetNoticed\BlockTestEnvironments\Controller\Bte;

use Magento\Framework\{
    App\Action\Action,
    Controller\ResultFactory
};

class Blocked extends Action
{
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $page */
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $page->getConfig()->getTitle()->set(__('You do not have permission to access this page.'));

        return $page;
    }
}
