# Get.Noticed - Block Test Environments

The **Block Test Environments** makes sure test / acceptance environments are only reachable for customers
that are supposed to be able to reach that location. Get.Noticed employees will always be able to reach an environment.

If a visitor reaches the website that is not supposed to be seeing that location, an error will be shown
with their remote address and the request to send an e-mail to support@getnoticed.nl to whitelist their address.

A complete issue template will be added to this page, so all the information that must be present is instantly visible.

## Mechanism

If the module is disabled, no special logic will be applied to any request. The webshop will operate as-is.

If the module is enabled, you must be whitelisted / authenticated in some way.
There are various mechanisms taking care of this.

### IP-whitelist

Currently the following IP-addresses are whitelisted by default:

* 159.100.68.190

To view the current IP-whitelist, run the CLI-command `php bin/magento bte:whitelist:view`.

More IP-addresses may be added through the CLI-command, `php bin/magento bte:whitelist:add <ip-address> <label>`.
IP-addresses may be removed through the CLI-command `php bin/magento bte:whitelist:remove <ip-address>`.

### Admin authentication

All users that are allowed to log into the admin panel are also allowed to see the front-end.

The way you must be authenticated works through the admin panel. If you log in to the admin, you'll see a button
that will bring you to the frontend with a cookie that authorizes you to temporarily view the frontend.

Because we can not actively check the admin lifetime, it'll be fairly limited (you'll need to re-authenticate daily).